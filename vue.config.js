module.exports = {
  pwa: {
    name: "holding-project",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "src/service-worker.js",
    },
  },
  transpileDependencies: ["vuetify"],
  devServer: {
    disableHostCheck: true,
    https: false
  } 
};
