import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import firebaseAuth from '../firebase';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/registro",
    name: "Register",
    component: Register,
  },
  {
    // path: "/trebol-holding/:id",
    path: "/trebol-holding",
    name: "Main",
    // meta: {requiresAuth: true},
    component: () =>
      import(/* webpackChunkName: "main" */ "../views/Main.vue")
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuthenticated = firebaseAuth.currentUser;
  if (requiresAuth && !isAuthenticated) next({ name: 'Login' })
  else next()
});

export default router;
