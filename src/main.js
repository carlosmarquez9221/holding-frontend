import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import firebaseAuth from './firebase';

Vue.config.productionTip = false;

let app;

firebaseAuth.onAuthStateChanged(user => {
  console.log(user)
  if(!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      render: (h) => h(App),
    }).$mount("#app"); 
  }
})
