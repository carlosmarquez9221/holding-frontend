import firebase from 'firebase'
import 'firebase/auth';

var firebaseConfig = {
  apiKey: "AIzaSyAHKVW2uzeSpTvVpGDpGomAAOE8FWkhr1I",
  authDomain: "holding-backend.firebaseapp.com",
  projectId: "holding-backend",
  storageBucket: "holding-backend.appspot.com",
  messagingSenderId: "688094127678",
  appId: "1:688094127678:web:29575aea463c065abad36f",
  measurementId: "G-TFYHMK70W0"
};

firebase.initializeApp(firebaseConfig);

// export const auth = firebase.auth();

export default firebase.auth()