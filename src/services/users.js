import axios from "axios";

const usuarios = {
  getUsers: params => axios.get(`${process.env.VUE_APP_API}/api/usuarios`, params)
}

export {
  usuarios
}