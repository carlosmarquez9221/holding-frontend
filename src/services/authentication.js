import firebaseAuth from '../firebase';

const Users = {
  loggedIn: false,

  activesession(){
    firebaseAuth.onAuthStateChanged(user => {
      if (user) this.loggedIn = true
      else this.loggedIn = false
    });
  },
  async register(email,password){
    try{
      const user = await firebaseAuth.createUserWithEmailAndPassword(email,password);
      return user
    } catch(error){
      console.log(error)
    }
  },
  async signOut(){
    try{
      const data = await firebaseAuth.signOut();
      console.log(data)
    }catch(error){  console.log(error) }
  },
  async signIn(email,password){
    try{
      const data = await firebaseAuth.signInWithEmailAndPassword(email,password)
      console.log(data)
      return data

    } catch(error){  console.log(error) }
  }
}

export { Users };