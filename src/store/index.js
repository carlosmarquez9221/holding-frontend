import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    template: {
      loading: false,
      panel_left: false,
      init_to_menu: false,
      header_display: true,
      label: "",
      app_class: "",
      header_type: ""
    },
    windowSize: { x: 0, y: 0, is_movile: true }
  },
  mutations: {
    setWindowSize(state, data) {
      if (data.x >= 980) data.is_movile = false;
      if (data.x < 980) data.is_movile = true;
      state.windowSize = data;
    },
    SetLoading(state, active) {
      state.template.loading = active;
    },
    TemplateActiveHeader(state, data) {
      state.template.header_display = true;
      state.template.header_type = data.header_type;
      state.template.label = data.label;
    },
    TemplateDisableHeader(state) {
      state.template.header_display = false;
    },
  },
  actions: {},
  modules: {},
});
